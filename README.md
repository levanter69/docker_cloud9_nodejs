# Docker base image for NodeJS remote coding using a local C9 IDE

Run your own Web IDE for NodeJS remote coding. It's based on cloud9 SDK and nodejs runtime.


To launch :
sudo docker run -d --restart=always --name=c9node1 -p 80:80 -p 15454:15454 -p 3000:3000 -v /cloud9/workspace:/workspace -e "C9USER=user" -e "C9PWD=password" levanter69/cloud9-nodejs


TODO :
Add SSL support
Detail docker command to launch mean stack
